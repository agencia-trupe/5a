<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterServicosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('servicos', function(Blueprint $table)
		{
			$table->integer('ordem')->default(-1)->after('imagem');
			$table->unique('titulo');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('servicos', function(Blueprint $table)
		{
			$table->dropColumn('ordem');
			$table->dropUnique('titulo');
		});
	}

}
