@section('content')

    <main id="diferenciais" class="center">
        <div class="cabecalho">
            <h2>Diferenciais</h2>
            <img src="{{ asset('../assets/img/cabecalhos/'.$cabecalho->diferenciais) }}" alt="">
        </div>

        <div class="content">
            <img src="{{ asset('../assets/img/diferenciais/'.$diferenciais->imagem) }}" alt="">
        </div>
    </main>
@stop