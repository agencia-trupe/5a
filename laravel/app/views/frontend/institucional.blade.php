@section('content')

    <main id="institucional" class="center">
        <div class="cabecalho">
            <h2>Institucional</h2>
            <img src="{{ asset('../assets/img/cabecalhos/'.$cabecalho->institucional) }}" alt="">
        </div>

        <div class="content {{ $pagina->pagina }}">
            <nav>
                @foreach($institucional as $menu)
                <a href="{{ route('institucional', $menu->pagina) }}"@if($pagina->pagina == $menu->pagina) class='active'@endif>{{ $menu->titulo }}</a>
                @endforeach
            </nav>

            <div class="texto">
                <div class="texto1">
                    {{ $pagina->texto1 }}
                </div>
                <div class="imagem">
                    <img src="{{ asset('../assets/img/institucional/'.$pagina->imagem) }}" alt="">
                </div>
                @if($pagina->texto2)
                <div class="texto2">
                    {{ $pagina->texto2 }}
                </div>
                @endif
            </div>
        </div>
    </main>
@stop