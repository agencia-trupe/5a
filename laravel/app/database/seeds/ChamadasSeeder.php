<?php

class ChamadasSeeder extends Seeder {

    public function run()
    {
        DB::table('chamadas')->delete();

        $data = array(
            array(
                'link'   => '/',
                'texto'  => 'Recrutamento e Seleção de Profissionais - TI',
                'imagem' => 'img.jpg'
            ),
            array(
                'link'   => '/',
                'texto'  => 'Recrutamento e Seleção de Profissionais - Hunting',
                'imagem' => 'img.jpg'
            ),
            array(
                'link'   => '/',
                'texto'  => 'Terceirização de Profissionais',
                'imagem' => 'img.jpg'
            ),
            array(
                'link'   => '/',
                'texto'  => 'Consultoria em Retenção de Talentos',
                'imagem' => 'img.jpg'
            )
        );

        DB::table('chamadas')->insert($data);
    }

}