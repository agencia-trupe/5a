@section('content')

    <legend>
        <h2><small>Home /</small> Editar Chamada</h2>
    </legend>

    {{ Form::model($chamada, [
        'route' => ['painel.chamadas.update', $chamada->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.chamadas._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop