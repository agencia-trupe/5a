<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsuariosSeeder');
		$this->call('ChamadasSeeder');
		$this->call('HomeSeeder');
		$this->call('CabecalhosSeeder');
		$this->call('InstitucionalSeeder');
		$this->call('DiferenciaisSeeder');
		$this->call('ServicosSeeder');
		$this->call('ContatoSeeder');
	}

}
