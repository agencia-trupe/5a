<?php

namespace Painel;

use \Cabecalho, \Input, \Validator, \Session, \Redirect, \CropImage;

class CabecalhosController extends BasePainelController {

    public function index()
    {
        $cabecalhos = Cabecalho::first();

        return $this->view('painel.cabecalhos.index', compact('cabecalhos'));
    }

    public function update($id)
    {
        $cabecalho = Cabecalho::findOrFail($id);
        $input     = Input::all();
        $rules     = Cabecalho::$rules;

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            foreach ($input as $key => $value) {
                if (Input::hasFile($key)) {
                    $input[$key] = CropImage::make($key, Cabecalho::$imagem_config);
                } else {
                    unset($input[$key]);
                }
            }

            $cabecalho->update($input);
            Session::flash('sucesso', 'Imagens alteradas com sucesso.');

            return Redirect::route('painel.cabecalhos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar imagens.'])
                ->withInput();

        }
    }

}