@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('texto1', 'Texto') }}
    {{ Form::textarea('texto1', null, ['class' => 'form-control ckeditor']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('imagem', 'Imagem')) }}
@if($submitText == 'Alterar')
    <img src="{{ url('../assets/img/institucional/'.$institucional->imagem) }}" style="display:block; margin-bottom: 10px; max-width:450px;height:auto;">
@endif
    {{ Form::file('imagem', ['class' => 'form-control']) }}
</div>

@if($institucional->pagina == 'descritivo')
<div class="form-group">
    {{ Form::label('texto2', 'Texto') }}
    {{ Form::textarea('texto2', null, ['class' => 'form-control ckeditor']) }}
</div>
@endif

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.institucional.index') }}" class="btn btn-default btn-voltar">Voltar</a>
