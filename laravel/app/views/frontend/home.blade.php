@section('content')

    <main id="home" class="center">
        <div class="banners-wrapper">
            @foreach($banners as $banner)
            <img src="{{ asset('../assets/img/banners/'.$banner->imagem) }}" alt="" class="banner">
            @endforeach
            <h3>{{ $frase->frase }}</h3>
        </div>

        <div class="chamadas">
            @foreach($chamadas as $chamada)
            <a href="{{ $chamada->link }}">
                <p>{{ $chamada->texto }}</p>
                <img src="{{ asset('../assets/img/chamadas/'.$chamada->imagem) }}" alt="">
            </a>
            @endforeach
        </div>

        <a href="http://5a.selecty.com.br/" target="_blank" class="vagas">
            <h3>Confira vagas disponíveis</h3>
            <p>Cadastre-se e faça parte de nosso banco de currículos para participar de nossos processos seletivos.</p>
            <span class="seta"></span>
        </a>
    </main>
@stop