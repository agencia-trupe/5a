<?php

return array(

    'name'        => '5A Gestão de Talentos',
    'title'       => '5A Gestão de Talentos | Pessoas fazem a diferença',
    'description' => 'A 5A ajuda organizações na conquista e superação de metas através da contatação dos melhores profissionais, gestão e retenção de talentos.',
    'keywords'    => 'arquitetura empresarial, especificação de software, integração de soluções, BPM multidimensional, arquitetura de negócio, arquitetura de informação, arquitetura de aplicações, arquitetura de tecnologia, modelagem de arquitetura empresarial, fabrica de teste de software, consultoria em QA Quality Assurance, Automação de testes, Testes em CMMi e TMMi, gestão estratégica, ferramenta de planejamento estratégico, software para planejamento estratégico, software que sistematiza o planejamento estratégico, controle de indicadores e KPI´s, sistema WMS, WMS gestão de portos, gestão de cadeia de frios, gestão de cadeia de varejo, gestão de processo de logística, fabrica de software digital, fabrica de aplicativos web, melhoria de performance e gestão, produtora de soluções digitais, fabrica de software com quality assurance QA, consultoria de TI, consultoria de profissionais de TI em são paulo, terceirização de profissionais de TI em são paulo, consultoria de recrutamento de profissionais de TI, consultoria de r&s em TI, consultoria de recrutamento em TI, consultoria de RH em TI, gestão em medicina ocupacinal, gestão em SST, gestão em Medicina do trabalho, gestão em segurança do trabalho, economia em SST, economia em Medicina do Trabalho, ferramenta para ppp, ppra, pcmso, epi, cursos online de TI, cursos online em comunicação corporativa, cursos online de comunicação empresarial, cursos de comunicação para empresas, cursos de TI incompany, cursos de comunicação incompany, cursos online corporativo',
    'share_image' => asset('../assets/img/layout/5a.png')

);
