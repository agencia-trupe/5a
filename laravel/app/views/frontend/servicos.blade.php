@section('content')

    <main id="servicos" class="center">
        <div class="cabecalho">
            <h2>Serviços</h2>
            <img src="{{ asset('../assets/img/cabecalhos/'.$cabecalho->servicos) }}" alt="">
        </div>

        <div class="content">
            <nav>
                @foreach($servicos as $menu)
                <a href="{{ route('servicos', $menu->pagina) }}"@if($pagina->pagina == $menu->pagina) class='active'@endif>{{ $menu->titulo }}</a>
                @endforeach
            </nav>

            <div class="texto">
                <h3>{{ $pagina->titulo }}</h3>
                <div class="texto1">
                    {{ $pagina->texto1 }}
                </div>
                @if($pagina->imagem)
                <div class="imagem">
                    <img src="{{ asset('../assets/img/servicos/'.$pagina->imagem) }}" alt="">
                </div>
                @endif
                @if($pagina->texto2)
                <div class="texto2">
                    {{ $pagina->texto2 }}
                </div>
                @endif
                @if($pagina->atuacao)
                <div class="atuacao">
                    <h5>Nossas áreas de atuação</h5>
                    {{ $pagina->atuacao }}
                </div>
                @endif
            </div>
        </div>
    </main>
@stop
