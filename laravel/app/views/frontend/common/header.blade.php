    <header>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ Config::get('projeto.title') }}</a></h1>
            <nav id="social">
                @if($contato->facebook)<a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>@endif
                @if($contato->twitter)<a href="{{ $contato->twitter }}" class="twitter" target="_blank">twitter</a>@endif
            </nav>
            <nav id="menu">
                <a href="{{ route('home') }}"@if(str_is('home', Route::currentRouteName())) class='active'@endif>home</a>
                <a href="{{ route('institucional') }}"@if(str_is('institucional*', Route::currentRouteName())) class='active'@endif>institucional</a>
                <a href="{{ route('diferenciais') }}"@if(str_is('diferenciais', Route::currentRouteName())) class='active'@endif>diferenciais</a>
                <a href="{{ route('servicos') }}"@if(str_is('servicos*', Route::currentRouteName())) class='active'@endif>serviços</a>
                <a href="http://5a.selecty.com.br/" target="_blank">vagas</a>
                <a href="{{ route('contato') }}"@if(str_is('contato', Route::currentRouteName())) class='active'@endif>contato</a>
            </nav>
        </div>
        <div class="nav-extension"></div>
    </header>