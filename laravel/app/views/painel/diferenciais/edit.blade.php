@section('content')

    <legend>
        <h2>Diferenciais</h2>
    </legend>

    {{ Form::model($diferenciais, [
        'route' => ['painel.diferenciais.update', $diferenciais->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.diferenciais._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop