<?php

class ServicosSeeder extends Seeder {

    public function run()
    {
        DB::table('servicos')->delete();

        $data = array(
            array(
                'pagina'  => 'recrutamento-e-selecao',
                'titulo'  => 'Recrutamento e Seleção',
                'texto1'  => '...',
                'texto2'  => '...',
                'atuacao' => '...',
                'imagem'  => 'img.jpg'
            ),
            array(
                'pagina'  => 'terceirizacao',
                'titulo'  => 'Terceirização',
                'texto1'  => '...',
                'texto2'  => '...',
                'atuacao' => '...',
                'imagem'  => 'img.jpg'
            ),
            array(
                'pagina'  => 'retencao-de-talentos',
                'titulo'  => 'Retenção de Talentos',
                'texto1'  => '...',
                'texto2'  => '...',
                'atuacao' => '...',
                'imagem'  => 'img.jpg'
            )
        );

        DB::table('servicos')->insert($data);
    }

}