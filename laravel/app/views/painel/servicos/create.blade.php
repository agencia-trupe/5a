@section('content')

    <legend>
        <h2><small>Serviços /</small> Adicionar Serviço</h2>
    </legend>

    {{ Form::open(['route' => 'painel.servicos.store', 'files' => true]) }}

        @include('painel.servicos._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
