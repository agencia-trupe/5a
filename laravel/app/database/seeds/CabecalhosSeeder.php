<?php

class CabecalhosSeeder extends Seeder {

    public function run()
    {
        DB::table('cabecalhos')->delete();

        $data = array(
            array(
                'institucional' => 'img.jpg',
                'diferenciais'  => 'img.jpg',
                'servicos'      => 'img.jpg',
                'contato'       => 'img.jpg'
            )
        );

        DB::table('cabecalhos')->insert($data);
    }

}