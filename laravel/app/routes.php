<?php

// Front-end

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('institucional/{pagina?}', [
    'as'   => 'institucional',
    'uses' => 'InstitucionalController@index'
]);

Route::get('diferenciais', [
    'as'   => 'diferenciais',
    'uses' => 'DiferenciaisController@index'
]);

Route::get('servicos/{pagina?}', [
    'as'   => 'servicos',
    'uses' => 'ServicosController@index'
]);

Route::get('contato', [
    'as'   => 'contato',
    'uses' => 'ContatoController@index'
]);
Route::post('contato', [
    'as'   => 'contato.send',
    'uses' => 'ContatoController@send'
]);


// Painel

Route::get('painel', [
    'before' => 'auth',
    'as'     => 'painel.home',
    'uses'   => 'Painel\HomeController@index'
]);

Route::get('painel/login', [
    'as'   => 'painel.login',
    'uses' => 'Painel\HomeController@login'
]);
Route::post('painel/login', [
    'as'   => 'painel.auth',
    'uses' => 'Painel\HomeController@attempt'
]);
Route::get('painel/logout', [
    'as'   => 'painel.logout',
    'uses' => 'Painel\HomeController@logout'
]);

Route::group(['prefix' => 'painel', 'before' => 'auth'], function() {
    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('frase', 'Painel\FraseController');
    Route::resource('banners', 'Painel\BannersController');
    Route::resource('chamadas', 'Painel\ChamadasController');
    Route::resource('cabecalhos', 'Painel\CabecalhosController');
    Route::resource('institucional', 'Painel\InstitucionalController');
    Route::resource('diferenciais', 'Painel\DiferenciaisController');
    Route::resource('servicos', 'Painel\ServicosController');
    Route::resource('contato/recebidos', 'Painel\ContatosRecebidosController');
    Route::resource('contato', 'Painel\ContatoController');
    Route::post('ajax/order', 'Painel\AjaxController@order');
});