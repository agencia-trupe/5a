<?php

class InstitucionalSeeder extends Seeder {

    public function run()
    {
        DB::table('institucional')->delete();

        $data = array(
            array(
                'pagina' => 'descritivo',
                'titulo' => 'Descritivo',
                'texto1' => '...',
                'texto2' => '...',
                'imagem' => 'img.jpg'
            ),
            array(
                'pagina' => 'missao',
                'titulo' => 'Missão',
                'texto1' => '...',
                'texto2' => '',
                'imagem' => 'img.jpg'
            ),
            array(
                'pagina' => 'valores',
                'titulo' => 'Valores',
                'texto1' => '...',
                'texto2' => '',
                'imagem' => 'img.jpg'
            ),
            array(
                'pagina' => 'origem-do-nome',
                'titulo' => 'Origem do nome',
                'texto1' => '...',
                'texto2' => '',
                'imagem' => 'img.jpg'
            )
        );

        DB::table('institucional')->insert($data);
    }

}