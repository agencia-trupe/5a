<?php

namespace Painel;

use \Frase, \Input, \Session, \Redirect, \Validator;

class FraseController extends BasePainelController {

    public function index()
    {
        $frase = Frase::first();

        return $this->view('painel.frase.edit', compact('frase'));
    }

    public function update($id)
    {
        $frase = Frase::findOrFail($id);
        $input = Input::all();

        $validate = Validator::make($input, Frase::$rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $frase->update($input);
            Session::flash('sucesso', 'Frase alterada com sucesso.');

            return Redirect::route('painel.frase.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar frase.'])
                ->withInput();

        }
    }

}