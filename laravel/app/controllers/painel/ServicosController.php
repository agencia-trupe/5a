<?php

namespace Painel;

use \Servicos, \View, \Input, \Session, \Redirect, \Validator, \CropImage, \Str;

class ServicosController extends BasePainelController {

    public function index()
    {
        $servicos = Servicos::ordenados()->get();

        return $this->view('painel.servicos.index', compact('servicos'));
    }

    public function create()
    {
        return $this->view('painel.servicos.create');
    }

    public function store()
    {
        $input = Input::all();
        $rules = Servicos::$rules;

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['pagina'] = Str::slug(Input::get('titulo'));
            $input['imagem'] = CropImage::make('imagem', Servicos::$imagem_config);

            Servicos::create($input);
            Session::flash('sucesso', 'Serviço criado com sucesso.');

            return Redirect::route('painel.servicos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar serviço. Verifique se já existe outro serviço com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $servicos = Servicos::findOrFail($id);

        return $this->view('painel.servicos.edit', compact('servicos'));
    }

    public function update($id)
    {
        $servicos = Servicos::findOrFail($id);
        $input    = Input::all();
        $rules    = Servicos::$rules;

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['pagina'] = Str::slug(Input::get('titulo'));

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', Servicos::$imagem_config);
            } else {
                unset($input['imagem']);
            }

            $servicos->update($input);
            Session::flash('sucesso', 'Serviço alterado com sucesso.');

            return Redirect::route('painel.servicos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar serviço. Verifique se já existe outro serviço com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Servicos::destroy($id);
            Session::flash('sucesso', 'Serviço removido com sucesso.');

            return Redirect::route('painel.servicos.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover serviço.']);

        }
    }

}
