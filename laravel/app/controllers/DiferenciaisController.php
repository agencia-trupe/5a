<?php

use \Diferenciais;

class DiferenciaisController extends BaseController {

	public function index()
	{
        $diferenciais = Diferenciais::first();

		return $this->view('frontend.diferenciais', compact('diferenciais'));
	}

}
