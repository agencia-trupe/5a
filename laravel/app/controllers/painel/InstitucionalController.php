<?php

namespace Painel;

use \Institucional, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class InstitucionalController extends BasePainelController {

    public function index()
    {
        $institucional = Institucional::all();

        return $this->view('painel.institucional.index', compact('institucional'));
    }

    public function edit($id)
    {
        $institucional = Institucional::findOrFail($id);

        return $this->view('painel.institucional.edit', compact('institucional'));
    }

    public function update($id)
    {
        $institucional = Institucional::findOrFail($id);
        $input         = Input::all();
        $rules         = Institucional::$rules;

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', Institucional::$imagem_config);
            } else {
                unset($input['imagem']);
            }

            $institucional->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.institucional.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}