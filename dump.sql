-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 26-Mar-2015 às 19:30
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `5a`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, '20150323171449_banner1.jpg', -1, '2015-03-23 20:14:50', '2015-03-23 20:14:50'),
(2, '20150323171458_banner2.jpg', -1, '2015-03-23 20:14:58', '2015-03-23 20:14:58');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cabecalhos`
--

CREATE TABLE IF NOT EXISTS `cabecalhos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `institucional` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diferenciais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `servicos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `cabecalhos`
--

INSERT INTO `cabecalhos` (`id`, `institucional`, `diferenciais`, `servicos`, `contato`, `created_at`, `updated_at`) VALUES
(1, '20150323171748_cabecalho-institucional.jpg', '20150323171748_cabecalho-diferenciais.jpg', '20150323171748_cabecalho-servicos.jpg', '20150323171748_cabecalho-contato.jpg', '0000-00-00 00:00:00', '2015-03-23 20:17:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamadas`
--

CREATE TABLE IF NOT EXISTS `chamadas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `chamadas`
--

INSERT INTO `chamadas` (`id`, `link`, `texto`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, '/servicos/recrutamento-e-selecao', 'Recrutamento e Seleção de Profissionais - TI', '20150323171602_chamada1.jpg', -1, '0000-00-00 00:00:00', '2015-03-24 17:14:35'),
(2, '/servicos/recrutamento-e-selecao', 'Recrutamento e Seleção de Profissionais - Hunting', '20150323171607_chamada2.jpg', -1, '0000-00-00 00:00:00', '2015-03-23 21:27:54'),
(3, '/servicos/terceirizacao', 'Terceirização de Profissionais', '20150323171611_chamada3.jpg', -1, '0000-00-00 00:00:00', '2015-03-23 21:28:01'),
(4, '/servicos/retencao-de-talentos', 'Consultoria em Retenção de Talentos', '20150323171615_chamada4.jpg', -1, '0000-00-00 00:00:00', '2015-03-23 21:28:17');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `googlemaps`, `facebook`, `twitter`, `created_at`, `updated_at`) VALUES
(1, 'contato@5a.com.br', '11 2344-4595', '<p>S&atilde;o Paulo</p>\r\n\r\n<p>Rua Arizona, 1426 - 4&ordm; andar</p>\r\n\r\n<p>Brooklin - 04567-003</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.835174729641!2d-46.694737700000005!3d-23.610243499999992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cc77c3e12d%3A0x84764471e584ebc!2sR.+Arizona%2C+1426+-+Cidade+Mon%C3%A7%C3%B5es%2C+S%C3%A3o+Paulo+-+SP%2C+04567-003!5e0!3m2!1spt-BR!2sbr!4v1427122411829" width="100%" height="100%" frameborder="0" style="border:0"></iframe>', '', 'https://twitter.com/5aconsultoria/', '0000-00-00 00:00:00', '2015-03-24 19:30:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `diferenciais`
--

CREATE TABLE IF NOT EXISTS `diferenciais` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `diferenciais`
--

INSERT INTO `diferenciais` (`id`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '20150323172246_diferenciais.png', '0000-00-00 00:00:00', '2015-03-23 20:22:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home`
--

CREATE TABLE IF NOT EXISTS `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frase` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `home`
--

INSERT INTO `home` (`id`, `frase`, `created_at`, `updated_at`) VALUES
(1, 'Ajudar as organizações na conquista e superação de suas metas através da contratação dos melhores profissionais, gestão e retenção de talentos é a proposta da 5A.', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `institucional`
--

CREATE TABLE IF NOT EXISTS `institucional` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto2` text COLLATE utf8_unicode_ci,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `institucional`
--

INSERT INTO `institucional` (`id`, `pagina`, `titulo`, `texto1`, `texto2`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'descritivo', 'Descritivo', '<p>Fundada em 2003, a 5A Gest&atilde;o de Talentos, empresa do Grupo 5A Company, consolidou-se no mercado como uma das principais refer&ecirc;ncias no segmento de contrata&ccedil;&atilde;o, terceiriza&ccedil;&atilde;o e reten&ccedil;&atilde;o de profissionais, devido ao seu Know-how em diferentes verticais de neg&oacute;cios e &agrave;s premissas inovadoras que alicer&ccedil;am os servi&ccedil;os prestados a grandes companhias.</p>\r\n\r\n<p>Diante da globaliza&ccedil;&atilde;o e da escassez de m&atilde;o de obra qualificada, a 5A Gest&atilde;o de Talentos apresenta-se como uma alternativa de sucesso para apoiar &agrave;s &aacute;reas de RH e de Tecnologia no desafio de montar e manter equipes de alta performance.&nbsp;</p>\r\n\r\n<p>Como acreditamos que o talento humano &eacute; o componente mais importante para uma organiza&ccedil;&atilde;o, nosso diferencial competitivo esta no mapeamento, relacionamento e fideliza&ccedil;&atilde;o deste talento, visando sua evolu&ccedil;&atilde;o t&eacute;cnica e comportamental.</p>\r\n\r\n<p>Visto que a opera&ccedil;&atilde;o de uma organiza&ccedil;&atilde;o depende de Pessoas, Processos e Tecnologia, ter uma boa equipe &eacute; o ponto de partida para o sucesso de uma organiza&ccedil;&atilde;o. Esta cren&ccedil;a refor&ccedil;a a filosofia e os servi&ccedil;os prestados pela 5A Gest&atilde;o de Talentos que se baseia no fato de que &ldquo;Pessoas fazem a diferen&ccedil;a&rdquo;.&nbsp;</p>\r\n', '<p>Com este conceito, a 5A se reafirma como empresa atuante nesse mundo em constante transforma&ccedil;&atilde;o e deixa claro seu objetivo no mercado: ter nascido para perpetuar, inovar e conectar as melhores empresas aos melhores profissionais.</p>\r\n\r\n<p>Com nossa filosofia de &ldquo;atender bem, para atender sempre&rdquo;, buscamos, a todo momento, superar as expectativas dos nossos clientes, fato que pode ser confirmado nas parcerias duradouras que estabelecemos.</p>\r\n', '20150323173010_descritivo.png', '0000-00-00 00:00:00', '2015-03-23 20:30:10'),
(2, 'missao', 'Missão', '<p>Apoiar nossos clientes no atendimento das suas metas atrav&eacute;s da presta&ccedil;&atilde;o de servi&ccedil;os de contrata&ccedil;&atilde;o, gest&atilde;o e reten&ccedil;&atilde;o de talentos.</p>\r\n', '', '20150323173121_missao.jpg', '0000-00-00 00:00:00', '2015-03-24 18:09:02'),
(3, 'valores', 'Valores', '<ul>\r\n	<li>Compromisso com o Cliente</li>\r\n	<li>Respeito &agrave;s pessoas</li>\r\n	<li>Transpar&ecirc;ncia nas atitudes</li>\r\n	<li>Perenidade nas rela&ccedil;&otilde;es</li>\r\n	<li>Foco na evolu&ccedil;&atilde;o cont&iacute;nua</li>\r\n	<li>Trabalho em equipe</li>\r\n</ul>\r\n', '', '20150323173159_valores.jpg', '0000-00-00 00:00:00', '2015-03-23 20:31:59'),
(4, 'origem-do-nome', 'Origem do nome', '<p>Para que uma empresa suportada por equipes alcance os resultados esperados, &eacute; necess&aacute;rio que o Talento Humano (ser humano) esteja orientado a manter a <strong>Aten&ccedil;&atilde;o </strong>no que faz e no que acontece ao seu redor, identificando as oportunidades e os obst&aacute;culos, dessa forma ter&aacute; possibilidade de criar <strong>Alternativas </strong>que aproveitem as oportunidades e superem os obst&aacute;culos. Uma Alternativa deve sempre ser acompanhada de uma boa dose de <strong>Atitude</strong>, permitindo assim a sua realiza&ccedil;&atilde;o, por sua vez, as Alternativas devem buscar um n&iacute;vel de <strong>Assertividade</strong>. Por&eacute;m de nada adiantar&aacute; a Aten&ccedil;&atilde;o, a gera&ccedil;&atilde;o de Alternativas, a Atitude e a Assertividade se o talento humano n&atilde;o <strong>Acreditar </strong>no que se prop&otilde;e a fazer na vida profissional e pessoal.</p>\r\n\r\n<p>Desta forma, estes 5 elementos que diferenciam um profissional de outro, s&atilde;o os elementos fundamentais que fazem parte do pr&oacute;prio nome da empresa.</p>\r\n', '', '20150323173347_origem.jpg', '0000-00-00 00:00:00', '2015-03-23 20:35:31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_28_130645_create_usuarios_table', 1),
('2015_03_23_121033_create_chamadas_table', 1),
('2015_03_23_121057_create_banners_table', 1),
('2015_03_23_121212_create_home_table', 1),
('2015_03_23_121325_create_institucional_table', 1),
('2015_03_23_121342_create_diferenciais_table', 1),
('2015_03_23_121410_create_servicos_table', 1),
('2015_03_23_121419_create_contato_table', 1),
('2015_03_23_121434_create_contatos_recebidos_table', 1),
('2015_03_23_151503_create_cabecalhos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto2` text COLLATE utf8_unicode_ci,
  `atuacao` text COLLATE utf8_unicode_ci,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `pagina`, `titulo`, `texto1`, `texto2`, `atuacao`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'recrutamento-e-selecao', 'Recrutamento e Seleção', '<p>Trabalhar no recrutamento e sele&ccedil;&atilde;o de profissionais com perfis complexos exige dom&iacute;nio das especifica&ccedil;&otilde;es, trabalho diferenciado e paix&atilde;o por superar as expectativas dos envolvidos.</p>\r\n\r\n<p>Para apoiar as &aacute;reas de RH a ampliar sua capacidade de entrega e a vencer os desafios promovidos pelo crescimento acelerado das organiza&ccedil;&otilde;es, a 5A oferece sua experi&ecirc;ncia e capacita&ccedil;&atilde;o na contrata&ccedil;&atilde;o de profissionais de forma&ccedil;&atilde;o t&eacute;cnica complexa.</p>\r\n', '<p>Nossa expertise em Recrutamento e Sele&ccedil;&atilde;o de profissionais especializados &eacute; focada na contrata&ccedil;&atilde;o do profissional certo no menor tempo poss&iacute;vel.</p>\r\n\r\n<p>Para que consigamos atender nossos clientes com velocidade e qualidade apresentamos alguns dos nossos diferenciais:</p>\r\n\r\n<ul>\r\n	<li>Time de Recrutamento e Sele&ccedil;&atilde;o altamente especializado na contrata&ccedil;&atilde;o de profissionais de diversas &aacute;reas;</li>\r\n	<li>Banco de Dados com mais de 40 mil profissionais que j&aacute; foram entrevistados na 5A;</li>\r\n	<li>Banco de Dados direcionado para as necessidades dos nossos clientes;</li>\r\n	<li>Forte rede de contatos;</li>\r\n	<li>Adequa&ccedil;&atilde;o do processo de Recrutamento e Sele&ccedil;&atilde;o &agrave;s necessidades dos nossos clientes (realiza&ccedil;&atilde;o de entrevista t&eacute;cnica e comportamental, aplica&ccedil;&atilde;o de testes e din&acirc;micas de grupo, entre outros);</li>\r\n	<li>Proximidade junto aos clientes e colaboradores.</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Tecnologia da informa&ccedil;&atilde;o</li>\r\n	<li>Projetos e Processos</li>\r\n	<li>Recursos Humanos</li>\r\n	<li>Engenharia, Log&iacute;stica e Transportes</li>\r\n	<li>Administrativo e Financeiro</li>\r\n	<li>Vendas e Marketing</li>\r\n</ul>\r\n', '20150323172403_recrutamento.png', '0000-00-00 00:00:00', '2015-03-23 20:24:03'),
(2, 'terceirizacao', 'Terceirização', '<p>Aliado a um processo de Recrutamento e Sele&ccedil;&atilde;o diferenciado, nosso modelo de gest&atilde;o e acompanhamento possibilita a aproxima&ccedil;&atilde;o das pessoas, estimulando a satisfa&ccedil;&atilde;o dos envolvidos e a reten&ccedil;&atilde;o dos talentos.</p>\r\n\r\n<p>Atuamos na contrata&ccedil;&atilde;o de profissionais por tempo determinado, buscando identificar o profissional que atenda &agrave;s necessidades t&eacute;cnicas e comportamentais dos nossos clientes e que possa se adaptar &agrave; cultura de cada organiza&ccedil;&atilde;o.</p>\r\n\r\n<p>Nosso objetivo &eacute; auxiliar nossos clientes no atendimento das suas metas, apoiando na montagem e gest&atilde;o de equipes especializadas, o que possibilita aos gestores o foco no seu neg&oacute;cio.</p>\r\n', '<p>Para que consigamos atender o mercado com velocidade e assertividade, apresentamos alguns dos nossos diferenciais:</p>\r\n\r\n<ul>\r\n	<li>Equipe e processo de recrutamento e sele&ccedil;&atilde;o especializados;</li>\r\n	<li>Amplo banco de dados para busca de talentos;</li>\r\n	<li>Sistema de atendimento que garante uma resposta r&aacute;pida e uma avalia&ccedil;&atilde;o de perfil direcionada para a necessidade de cada projeto/empresa;</li>\r\n	<li>Metodologia de gest&atilde;o de pessoas que minimiza a dificuldade de integra&ccedil;&atilde;o do profissional na equipe do projeto e ap&oacute;ia o cliente na gest&atilde;o comportamental do colaborador;</li>\r\n	<li>Equipe de especialistas em Gest&atilde;o de Pessoas para suporte no processo de acompanhamento dos profissionais;</li>\r\n	<li>Processo de fideliza&ccedil;&atilde;o e reten&ccedil;&atilde;o dos talentos;</li>\r\n	<li>Avalia&ccedil;&otilde;es peri&oacute;dicas - Indicadores de satisfa&ccedil;&atilde;o (cliente e colaborador)</li>\r\n	<li>Especializa&ccedil;&atilde;o no atendimento de demandas de todas as &aacute;reas de TI, Projetos, Processos e RH.</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Tecnologia da informa&ccedil;&atilde;o</li>\r\n	<li>Projetos e Processos</li>\r\n	<li>Recursos Humanos</li>\r\n</ul>\r\n', '20150323172737_terceirizacao.png', '0000-00-00 00:00:00', '2015-03-23 20:27:37'),
(3, 'retencao-de-talentos', 'Retenção de Talentos', '<p>Muito se fala sobre a escassez de profissionais e a dificuldade encontrada no processo de contrata&ccedil;&atilde;o. Entretanto, com o ritmo acelerado do mercado e com a demanda maior do que a quantidade de profissionais qualificados dispon&iacute;veis, a manuten&ccedil;&atilde;o da equipe tornou-se um grande desafio para os gestores.</p>\r\n\r\n<p>O processo de recrutamento e sele&ccedil;&atilde;o pode ser definido como o processo de escolher a pessoa certa para o lugar certo. J&aacute; a Reten&ccedil;&atilde;o de Talentos, pode ser definida como um processo que abrange um conjunto de a&ccedil;&otilde;es que permeia toda o ciclo de perman&ecirc;ncia do profissional dentro de uma organiza&ccedil;&atilde;o.</p>\r\n', '<p>Para que consigamos auxiliar nossos clientes na Reten&ccedil;&atilde;o dos seus Talentos, apresentamos alguns dos nossos diferenciais:</p>\r\n\r\n<ul>\r\n	<li>Equipe de especialistas em Gest&atilde;o de Pessoas para desenvolvimento de solu&ccedil;&otilde;es adequadas &agrave; realidade e Cultura de cada organiza&ccedil;&atilde;o;</li>\r\n	<li>Dom&iacute;nio dos conceitos de R&amp;S, Integra&ccedil;&atilde;o e Motiva&ccedil;&atilde;o de equipes;</li>\r\n	<li>Relacionamento com diversas institui&ccedil;&otilde;es de ensino e especialistas em capacita&ccedil;&atilde;o;</li>\r\n	<li>Metodologia de gest&atilde;o de pessoas que minimiza a dificuldade de integra&ccedil;&atilde;o do profissional na equipe dos projetos e que ap&oacute;ia o cliente na gest&atilde;o comportamental dos seus colaboradores;</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Recrutamento e Sele&ccedil;&atilde;o</li>\r\n	<li>Capacita&ccedil;&atilde;o</li>\r\n	<li>Integra&ccedil;&atilde;o e Motiva&ccedil;&atilde;o de Equipe</li>\r\n	<li>Pesquisas de Satisfa&ccedil;&atilde;o e de Clima Organizacional</li>\r\n	<li>Gest&atilde;o da Transforma&ccedil;&atilde;o</li>\r\n</ul>\r\n', '20150323172911_retencao.png', '0000-00-00 00:00:00', '2015-03-23 20:29:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_unique` (`email`),
  UNIQUE KEY `usuarios_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', 'trupe', '$2y$10$9hB423rQkEOz4boBt486l.8cUtX4oMY4JQA.zUzmCDjiUU8Gn3fEG', 'TUs6fGAXhONS6nhDJzsYMVYEviaziZeJtdMnQ5WHwVjiCCtKHe0aTAllUgao', '0000-00-00 00:00:00', '2015-03-24 19:33:32');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
