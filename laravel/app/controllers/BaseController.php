<?php

class BaseController extends Controller {

	protected $layout = 'frontend.common.template';

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
            $contato = \Contato::first();
            $cabecalho = \Cabecalho::first();
            View::share(compact('contato', 'cabecalho'));

			$this->layout = View::make($this->layout);
		}
	}

    protected function view($path, array $data = [])
    {
        $this->layout->content = View::make($path, $data);
    }

}
