<?php

use \Banner, \Chamada, \Frase;

class HomeController extends BaseController {

	public function index()
	{
        $chamadas = Chamada::ordenados()->get();
        $banners  = Banner::ordenados()->get();
        $frase    = Frase::first();

		return $this->view('frontend.home', compact('chamadas', 'banners', 'frase'));
	}

}
